require("./style.css");
document.write(require("./content.js"));


var innerArray = [5,6,7];

var fullArray = [1, 2, ...innerArray, 9, ...innerArray, 10];

console.log(fullArray);

function callMe() {
    return "WORLD";
}

console.log(`Hello ${callMe()}`);

setTimeout(() => {
    var a = Array.from(document.querySelectorAll('.abc'));
    console.log([1, 2, ...a, 6]);
}, 10);

var foo = {
  [Symbol.iterator]: () => ({
    items: ['p', 'o', 'n', 'y', 'f', 'o', 'o'],
    next: function next () {
      return {
        done: this.items.length === 0,
        value: this.items.shift()
      }
    }
  })
}

function* foo_generator() {
    for (let element of ['p', 'o', 'n', 'y', 'f', 'o', 'o']) yield element + "EZ";
}

for (var item of foo) {
    console.log(item);
}

var itr = foo_generator();

for (var element of itr) {
    console.log("elemcsi", element);
}

function request(url) {
    return new Promise((resolve, reject) => {
        var req = new XMLHttpRequest();
        
        req.onreadystatechange = function() {
            if (req.readyState == 4 && req.status == 200) {
                resolve(req.responseText);
            } else if (req.readyState == 4 && req.status == 404) {
                reject(req.responseText);
            }
        }
        
        req.open("GET", url, true);
        req.send();
    });
};


request("myFile.txt")
    .then((value) => console.log(`Result: ${value}`))
    .catch((error) => console.log(`Error: ${error}`));
    
    
fetch("myFile.txt")
    .then(result => console.log(`fetch RESULT: ${result}`))
    .catch(error => console.log(`fetch ERROR: ${error}`))
    
    
    
    
var m = new Map();

m.set("key1", { value: 10 });
m.set("key2", { value: 20 });
m.set("key3", { value: 30 });
m.set("key4", { value: 40 });

for (let [key, element] of m) {
    console.log(`KEY: ${key}, VALUE: ${element.value}`);
}

console.log(...m);