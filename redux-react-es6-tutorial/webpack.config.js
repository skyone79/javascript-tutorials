var path = require('path');

module.exports = {
    entry: "./app/index.js",
    output: {
        path: path.resolve(__dirname, "build"),
        filename: "todo-app.js"
    },
    devtool: 'inline-source-map',
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel",
                query: {
                    presets: ["es2015", "react"]
                }
            }
        ]
    }
}