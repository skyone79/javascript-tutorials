export const ADD_TODO = 'ADD_TODO';
export const TOGGLE_TODO = 'TOGGLE_TODO';

export const toggleTodo = ({ id }) => {
    return {
        type: TOGGLE_TODO,
        id
    }
}

export const addTodo = ({ id, text }) => {
    return {
        type: ADD_TODO,
        id,
        text
    }
}