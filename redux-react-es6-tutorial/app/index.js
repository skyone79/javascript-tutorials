import React from 'react';
import ReactDOM from 'react-dom';
import { bindActionCreators } from 'redux';
import { Provider, connect } from 'react-redux';
import store from './store/configureStore';
import TodoApp from './components/TodoApp';
import * as todoActionCreators from './actions/todoActions';
import * as visibilityFilterActionCreators from './actions/visibilityFilterActions';

const mapStateToProps = (state) => { 
    return {
        todos: state.todos,
        visibilityFilter: state.visibilityFilter
    }
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        Object.assign({}, todoActionCreators, visibilityFilterActionCreators), 
        dispatch
    );
}

const WiredTodoApp = connect(mapStateToProps, mapDispatchToProps)(TodoApp);

ReactDOM.render(
    <Provider store={store}>
        <WiredTodoApp />
    </Provider>,
    document.getElementById("app")
);