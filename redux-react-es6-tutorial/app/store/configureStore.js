import { createStore, applyMiddleware, compose } from 'redux';
import logger from './logger';
import reducers from '../reducers';

const finalCreateStore = compose(
	applyMiddleware(logger)
)(createStore);

export default finalCreateStore(reducers);