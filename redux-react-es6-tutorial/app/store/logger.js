export default store => next => action => {
    console.info('will dispatch', action);

    // Call the next dispatch method in the middleware chain.
    let returnValue = next(action);

    console.info('state after dispatch', store.getState());

    return returnValue;
}