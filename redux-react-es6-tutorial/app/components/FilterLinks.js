import React from 'react';
import { SHOW_ALL, SHOW_ACTIVE, SHOW_COMPLETED } from '../actions/visibilityFilterActions';

const FilterLinks = ({ visibilityFilter, setVisibilityFilter }) => {
    return (
        <p>
            Show:
            <FilterLink 
                filter={ SHOW_ALL }
                currentFilter={ visibilityFilter }
                clickHandler={ setVisibilityFilter }
                >
                All
            </FilterLink>{" "}
            <FilterLink 
                filter={ SHOW_ACTIVE }
                currentFilter={ visibilityFilter }
                clickHandler={ setVisibilityFilter }
                >
                Active
            </FilterLink>{" "}
            <FilterLink 
                filter={ SHOW_COMPLETED }
                currentFilter={ visibilityFilter }
                clickHandler={ setVisibilityFilter }
                >
                Completed
            </FilterLink>{" "}
        </p>
    )
}

const FilterLink = ({ filter, currentFilter, children, clickHandler }) => {
    if (filter === currentFilter) {
        return <span>{ children }</span>;
    }
    
    return (
        <a href="#" onClick={ event => clickHandler(filter) }>
            { children }
        </a>
    )
}

export default FilterLinks;