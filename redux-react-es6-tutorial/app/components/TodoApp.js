import React, { Component } from 'react';
import AddTodo from './AddTodo';
import TodoList from './TodoList';
import FilterLinks from './FilterLinks';

export default class TodoApp extends Component {
    render() {
        return (
            <div>
                <AddTodo { ...this.props } />
                <TodoList { ...this.props } />
                <FilterLinks { ...this.props } />
            </div>
        );
    }
}