import React, { Component } from 'react';
import { SHOW_ALL, SHOW_COMPLETED, SHOW_ACTIVE } from '../actions/visibilityFilterActions';

export default class TodoList extends Component {
    onToggleTodo(todo) {
        this.props.toggleTodo({
            id: todo.id
        });
    }
    
    getVisibleTodos(todos, filter) {
        switch (filter) {
            case SHOW_ALL:
                return todos;

            case SHOW_COMPLETED:
                return todos.filter(todo => todo.completed);
                
            case SHOW_ACTIVE:
                return todos.filter(todo => !todo.completed);
        }
    }
    
    render() {
        const {todos, visibilityFilter} = this.props;
        
        const visibleTodos = this.getVisibleTodos(todos, visibilityFilter);
        
        return (
            <ul>
                {visibleTodos.map(todo => {
                    return (
                        <Todo 
                            key={todo.id}
                            clickHandler={this.onToggleTodo.bind(this, todo)}
                            completed={todo.completed}
                            text={todo.text}
                            />
                    );
                })}
            </ul>
        );
    }
}

const Todo = ({ clickHandler, completed, text }) => {
    return (
        <li 
            onClick={ clickHandler }
            style={{textDecoration: completed ? "line-through" : "none"}}
            >
            { text }
        </li>
    )
}
