import React, { Component } from "react";

export default class AddTodo extends Component {
    constructor() {
        super();
        
        this.currentInput = null;
        this.incrementalId = 0;
    }

    onAddClick() {
        this.props.addTodo({
            text: this.currentInput.value,
            id: this.incrementalId++
        })
        
        this.currentInput.value = "";
    }
    
    render() {
        return (
            <div>
                <input ref={node => {
                    this.currentInput = node;
                }} />
                <button 
                    onClick={ this.onAddClick.bind(this) }
                    >
                    Add Todo
                </button>
            </div>
        );
    }
}