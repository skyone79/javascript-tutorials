import React, { Component } from "react";
import ReactDOM from "react-dom";

class App extends Component {
    constructor() {
        super();
        this.state = { 
            red: 0,
            green: 0,
            blue: 0
        };
        this.update = this.update.bind(this);
    }
    
    update(e) {
        this.setState({ 
            red: ReactDOM.findDOMNode(this.refs.red).value,
            green: ReactDOM.findDOMNode(this.refs.green).value,
            blue: ReactDOM.findDOMNode(this.refs.blue).value 
        });
    }
    
    render() {
        return (
            <div>
                <Slider ref="red" update={ this.update } />
                <h3>{ this.state.red }</h3>
                <br />
                <Slider ref="green" update={ this.update } />
                <h3>{ this.state.green }</h3>
                <br />
                <Slider ref="blue" update={ this.update } />
                <h3>{ this.state.blue }</h3>
                <br />
                <Button txt="ddss" />
            </div>
        );
    }
}

class Slider extends Component {
    render() {
        return (
            <input 
                type="range" 
                min="5" 
                max="120" 
                onChange={ this.props.update } 
                />
        );
    }
}

class Button extends Component {
    render() {
        return <button>{ this.props.txt }</button>
    }
}

ReactDOM.render(
    <App />, 
    document.getElementById("app")
);