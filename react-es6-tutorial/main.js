import App_Redux from "./App_Redux";

// Independent learn activities
import expect from "expect";

let obj1 = {
    "a": 1
}

let obj2 = {
    "b": 2
}

let obj3 = {
    "c": 3
}

let obj4 = {
    "ee": 11
}

let final = Object.assign(obj4, obj2, obj1, obj3, {suty: "platty"});

console.log(final);

expect(
    12
).toEqual(12);

console.log(window.innerWidth, window.innerHeight);
console.log(document.body.clientWidth, document.body.clientHeight);


let author1 = { name: "Sam" };
let author2 = { name: "Tyler" };

let mostRecentReply = {};

mostRecentReply[author1] = "ES2015";
mostRecentReply[author2] = "Semi-colons: Good or Bad?"


console.log( mostRecentReply[author1] );
console.log( mostRecentReply[author2] );


let mappifiedMostRecentReply = new Map();
mappifiedMostRecentReply.set(author1, "ES2015");
mappifiedMostRecentReply.set(author2, "Semi-colons: Good or Bad?");

console.log( mappifiedMostRecentReply.get(author1) );
console.log( mappifiedMostRecentReply.get(author2) );



const secret = ({ msg = "ES6 rocks!" } = {}) => () => msg;

console.log(secret()());