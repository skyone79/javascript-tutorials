import React, { Component } from "react";
import ReactDOM from "react-dom";

class Common extends Component {
    constructor() {
        super();
        this.state ={
            value: 0
        };
    }
    
    componentWillMount() {
        console.log("...MOUNTING");
    }
    
    shouldComponentUpdate(nextProps, nextState) {
        return Math.random() < 0.5;
    }
    
    onMouseClick() {
        this.setState({
            value : this.state.value + 1
        });
        console.log("....", this.state.value);
    }
    
    componentDidMount() {
        console.log("...MOUNTED");
    }
    
    componentWillUnmount() {
        console.log("...UNMMOUNTING");
    }
}

class Button extends Common {
    render() {
        console.log("...RENDER");
        
        return (
            <button onClick={ this.onMouseClick.bind(this) }>
                Current value: { this.state.value }
            </button>
        );
    }
}

class Label extends Common {
    render() {
        console.log("...RENDER");
        
        return (
            <label onClick={ this.onMouseClick.bind(this) }>
                Current value: { this.state.value }
            </label>
        );
    }
}

class App_1 extends Component {
    constructor() {
        super();
    }
    
    mount() {
        ReactDOM.render(<Button />, document.getElementById("place"));
        ReactDOM.render(<Label />, document.getElementById("place2"));
    }
    
    unmount() {
        ReactDOM.unmountComponentAtNode(document.getElementById("place"));
        ReactDOM.unmountComponentAtNode(document.getElementById("place2"));
    }
    
    render() {
        return (
            <div>
                <button onClick={ this.mount }>Mount</button>
                <button onClick={ this.unmount }>Unmount</button>
                <div id="place"></div>
                <div id="place2"></div>
            </div>
        );
    }
}

ReactDOM.render(
    <App_1 />, 
    document.getElementById("app")
);