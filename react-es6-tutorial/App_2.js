import React, { Component } from "react";
import ReactDOM from "react-dom";

export default class App_2 extends Component {
    constructor() {
        super();
        this.state = {
            data: [
                {id: 1, name: "Sanyi"},
                {id: 2, name: "Pityu"},
                {id: 3, name: "Rezso"},
                {id: 4, name: "Kazmer"},
                {id: 5, name: "Bottyan"},
                {id: 6, name: "Botond"},
                {id: 7, name: "Jeno"},
                {id: 8, name: "Zsiga"},
                {id: 9, name: "Pali"}
            ]
        };
    }
    
    render() {
        let rows = this.state.data.map(person => {
            return <PersonRow key={ person.id } data={ person } />
        });
        console.log("....RENDERING");
        return (
            <table>
                <tbody>{ rows }</tbody>
            </table>
        );
    }
}

const PersonRow = person => {
    return (
        <tr>
            <td>{ person.data.id }</td>
            <td>{ person.data.name }</td>
        </tr>
    );
}

ReactDOM.render(<App_2 />, document.getElementById("app"));