import React, { Component } from "react";
import ReactDOM from "react-dom";
import { createStore } from "redux";
import {Provider, connect} from "react-redux";

const INCREASE = "INCREASE";
const DECREASE = "DECREASE";

const store = createStore((state = 0, action) => {
    switch (action.type) {
        case INCREASE:
            return state  + 1;
        case DECREASE:
            return state - 1;
        default:
            return state;
    }
});

const increment = () => {
    return {
        type: INCREASE
    }
}

const decrement = () => {
    return {
        type: DECREASE
    }
}

class ReduxAppBase extends Component {
    constructor() {
        super();
    }
    
    render() {
        return (
            <div>
                <Counter {...this.props} />
            </div>
        );
    }
}

class Counter extends Component {
    render() {
        return (
            <div>
                <button onClick={this.props.increment}>Increase</button>
                <button onClick={this.props.decrement}>Decrease</button>
                <h1>{ this.props.state }</h1>
            </div>
        );
    }
};

const mapStateToProps = (state) => { 
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        increment: () => {
            dispatch(increment());
        },
        decrement: () => {
            dispatch(decrement())
        }
    }
};

const ReduxApp = connect(mapStateToProps, mapDispatchToProps)(ReduxAppBase);

class App extends Component {
    render() {
        return (
            <Provider store={ store }>
                <ReduxApp />
            </Provider>
        );
    }
}

ReactDOM.render(<ReduxApp store={ store } />, document.getElementById("app"));